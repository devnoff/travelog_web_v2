define([
  'react',
  'api',
  'mod/LayoutReadySubMixin',
  'facebook'
], function(React, api, LayoutReadySubMixin, FB){

  // Image JSX Helper Mixin
  var ImageJSXMixin = {
    getImageComp: function(image) {

      if (!image) {
        return '';
      }

      var coverAuthor = '';
      var coverImage = '';
      if (image.author.name) {
          coverAuthor = <div className="author">{image.author}</div>; 
        }
      coverImage = <img src={image.url}/>;

      return <div className="cover-image">{coverImage}{coverAuthor}</div>
    }
  };

  // Description Box
  var DescriptionBox = React.createClass({
    mixins: [ImageJSXMixin],

    propTypes: {
      cover_image: React.PropTypes.object,
      description: React.PropTypes.string
    },

    render: function() {
      
      var coverImage = this.getImageComp(this.props.cover_image);
      var description = this.props.description ? <p>{this.props.description}</p> : '';

      return(
        <div className="description">
          {coverImage}
          {description}
        </div>
      );
    }
  });

  // Place Box
  var PlaceBox = React.createClass({
    mixins: [ImageJSXMixin],

    propTypes: {
      place: React.PropTypes.object
    },

    render: function() {
      var place = this.props.place;
      var coverImage = this.getImageComp(place.image);
      return(
        <div className="place">
          {coverImage}
          <h3>{place.title}</h3>
          <p>{place.description}</p>
        </div>
      );
    }
  });

  // Subtitle Box
  var SubTitleBox = React.createClass({
    propTypes: {
      text: React.PropTypes.string
    },
    render: function() {
      return(
        <div className="subttl">
          <h2>{this.props.text}</h2>
        </div>
      );
    }
  });

  // Text Box
  var TextBox = React.createClass({
    propTypes: {
      text: React.PropTypes.string
    },
    render: function() {
      return(
        <div className="text">
          <p>{this.props.text}</p>
        </div>
      );
    }
  });

  // Photo Box
  var PhotoBox = React.createClass({
    mixins: [ImageJSXMixin],

    propTypes: {
      image: React.PropTypes.object
    },

    render: function() {
      var coverImage = this.getImageComp(this.props.image);
      return(
        <div className="photo">
          {coverImage}
        </div>
      );
    }
  });

  // Link Box
  var LinkBox = React.createClass({
    propTypes: {
      title: React.PropTypes.string,
      url: React.PropTypes.string
    },

    render: function() {
      return(
        <div className="link">
          <a href={this.props.url} target="_blank">{this.props.title}</a>
        </div>
      );
    }
  });  


  var ContentBox = React.createClass({
    propTypes: {
      contents: React.PropTypes.array
    },

    render: function() {

      var items = [];
      if (this.props.contents) {
        this.props.contents.forEach(function(item){
          if (item.type == 'place') {
            items.push(<PlaceBox place={item} key={item.seq} />);

          } else if (item.type == 'title') {
            items.push(<SubTitleBox text={item.text} key={item.seq} />);

          } else if (item.type == 'text') {
            items.push(<TextBox text={item.text} key={item.seq} />);

          } else if (item.type == 'photo') {
            items.push(<PhotoBox image={item.image} key={item.seq} />);

          } else if (item.type == 'link') {
            items.push(<TextBox title={item.title} url={item.url} key={item.seq} />);
          }
        }.bind(this));
      }

      var description = this.props.cover_image && this.props.description ? <DescriptionBox cover_image={this.props.cover_image} description={this.props.description} /> : '';

      return(
        <div className="contents">
          {description}
          {items}  
        </div>
      );
    }
  });

  var ArticleBox = React.createClass({
    mixins: [LayoutReadySubMixin],

    getInitialState: function() {
      return {
        page_url: false
      };
    },

    finishLoading: function() {
      this.subModuleLayoutReady(true);
    },

    componentDidMount: function() {

    },

    componentDidUpdate: function() {

      var $h = $(React.findDOMNode(this.refs.header));
      this.props.onReady($h.height());

      this.subModuleLayoutReady(true);

      FB.XFBML.parse();
    },

    render: function(){
      var article = this.props.article;
      var title = article.title;
      var author = article.display_author ? article.display_author.toUpperCase() : '';
      var date = article.display_date;
      var city_label = article.city ? <div className="ui mini label">{article.city.name.toUpperCase()}</div> : '';
      var page_url = this.state.page_url;

      var fbLikeBtn = page_url ? <div className="fb-like" data-href={page_url} data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div> : '';
      var fbCommentBox = page_url ? <div className="fb-comments" data-width="500" data-href={page_url} data-numposts="3" data-colorscheme="light" data-version="v2.3"></div> : '';


      return(
        <article>
          <div ref="header" className="header">
            <h1>{title}</h1>
            <div>
              <span className="author">BY {author} </span> 
              <span className="date">{date} </span>
              {fbLikeBtn}
            </div>
            <div className="tags">
              {city_label}
            </div>
          </div>
          <ContentBox contents={article.contents} cover_image={article.cover_image} description={article.description} /> {/* Article Content */}
          {fbCommentBox}
        </article>
      );
    }
  });

  return ArticleBox;
});