define([
    'jquery',
    'react',
    'api',
    'jsx!mod/ArticleBox',
    'jsx!mod/ArticleListBox',
    'jsx!mod/NewsFeedBox',
    'mod/LayoutReadyMixin',
    'css!css_/article.section',
    'css!css_/article.list.box',
    'css!css_/newsfeed.section',
    'css!css_/promote.box'
  ],
function($, React, api, ArticleBox, ArticleListBox, NewsFeedBox, LayoutReadyMixin){

  var ArticlePage = React.createClass({
    mixins: [LayoutReadyMixin],

    propTypes: {
      article_id: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.number
      ])
    },

    getInitialState: function() {
      return {
        article: {}
      };
    },

    componentDidMount: function() {
      // for Layout Ready
      this.addSubModule(this.refs.more_article);
      this.addSubModule(this.refs.article_box);


      this.loadArtlceData();
    },

    componentDidUpdate: function() {
      this.updateURI();
    },

    onArticleBoxReady: function(header_height) {
      // Adjust Top Margin of Side Item
      var $side = $(React.findDOMNode(this.refs.side));
      $side.css({
        'margin-top': header_height + 16
      });

      console.log('header height: ' + header_height);
    },

    loadArtlceData: function() {
      var url = api.baseUrl + 'article/' + this.props.article_id;
      $.ajax({
        url: url,
        dataType: 'json',
        success: function(data) {
          this.setState({
            article: data.data
          });
          
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(url, status, err.toString());
        }.bind(this)
      });
    },

    getPageURL: function() {
      return window.location.href;
    },

    updateURI: function(){
      var title = this.state.article.title;

      if (title) {
        var arr = title.split(' ');
        title = arr.join('-');

        console.log('updateURI' + title);

        var currentState = window.history.state;
        window.backbone_router.navigate("article/"+this.state.article.id+"/"+title, {trigger: false, replace: true});
        this.onUpdateURI();
      }
    },

    onUpdateURI: function() {
      this.refs.article_box.setState({
        page_url: window.location.href
      });
    },

    render: function(){

      var city = this.state.article.city ? this.state.article.city.name : {};
      var source = this.state.article.display_author ? this.state.article.display_author : '';

      var more_article_api_url = api.baseUrl + 'article/' + this.props.article_id + '/related_articles';
      var more_article_params = {
        ref_type: 'city',
        order_by: 'published_at',
        limit: '6'
      };

      var more_of_source_api_url = api.baseUrl + 'article/' + this.props.article_id + '/related_articles';
      var more_of_source_params = {
        ref_type: 'source',
        order_by: 'published_at',
        limit: '6'
      };

      return(
        <div>
          <section className="article">
            <div className="wrapper">
              <div className="left-container">
                <ArticleBox ref="article_box" article={this.state.article} onReady={this.onArticleBoxReady} pageURL={this.getPageURL}/> { /* Article Box */ }
              </div>
              <aside ref="side" className="right-container">
                <div className="article-list-box">
                    <h2>MORE ARTICLES in {city}</h2>
                    <ArticleListBox ref="more_article" url={more_article_api_url} params={more_article_params} thumb={true} /> { /* More Article List Box */ }
                </div>
                <br/>
                <div className="article-list-box">
                    <h2>MORE from {source.toUpperCase()}</h2>
                    <ArticleListBox ref="more_article" url={more_of_source_api_url} params={more_of_source_params} thumb={true} /> { /* More Article List Box */ }
                </div>
                <div className="download-app-box">
                    <span className="title">DOWNLOAD OUR APP</span>
                    <span className="description">HERE'S A SHORTCUT TO THE BEST PLACE TO STAY, EAT AND PLAY IN ASIA</span>
                    <div className="ui button">
                        <i className="apple icon"></i>GET THE APP
                    </div>
                </div>
                <div className="subscribe-promote-box">
                    <span className="description">SUBSCRIBE TO OUR NEWSLETTER & RECIEVE UPDATES RIGHT IN YOUR INBOX!</span>
                    <div className="ui mini left icon input">
                        <input type="text" placeholder="Email address" />
                        <i className="mail icon"></i>
                    </div>
                    <div className="ui travelog tiny subscribe button">SUBSCRIBE</div>
                </div> 
              </aside>
            </div>
          </section>
          <section className="newsfeed">
            <NewsFeedBox ref="news_feed_box" /> { /* News Feed Box */ }
          </section>
        </div>
      );
    }
  });
  
  return ArticlePage;
})