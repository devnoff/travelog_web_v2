define([
  'jquery',
  'react',
  'mod/LayoutReadySubMixin'
], function($, React, LayoutReadySubMixin){
  var ArticleItem = React.createClass({
    getDefaultProps : function() {
      return {
        thumb: false
      };
    },

    render: function() {
      
      var d = this.props.article.source_date;
      var date = '';
      if (d && d !== null && d !== undefined) {
        date = new Date(this.props.article.source_date); 
        date = date.toLocaleDateString();
      }

      var id = this.props.article.id;


      var liStyle = {
        backgroundImage: 'url(' + this.props.article.cover_image.url + ')'
      };
      var thumb = this.props.thumb ? (<div className="thumb" style={liStyle}></div>) : '';
      var contentStyle = thumb ? 'with-thumb' : '';
      console.log(this.props.thumb + ' thumb');
      return(
        <li>
            <div>
              <div className="ui mini city label">{this.props.article.city.name.toUpperCase()}</div>
              <span className="hours">{date}</span>
            </div>
            {thumb}
            <div className={contentStyle}>
              <a className="title" href={"/article/" + id}>{this.props.article.title}</a>
              <a className="publisher">{this.props.article.source}</a>
            </div>
        </li>
      );
    }
  });

  var ArticleListBox = React.createClass({
    mixins: [LayoutReadySubMixin],

    getInitialState: function() {
      return {
        data: []
      };
    },

    finishLoading: function() {
      this.subModuleLayoutReady(true);
    },

    loadData: function() {

      if (!this.enableLayoutReadyMixin) { this.showLoader(); }
      
      var url = this.props.url;

      $.ajax({
        url: url,
        data: this.props.params,
        dataType: 'json',
        success: function(data) {
          this.finishLoading();
          this.setState({
            data: data.data
          });

          if (!this.enableLayoutReadyMixin) { this.hideLoader(); }
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(url, status, err.toString());
          this.finishLoading();
          if (!this.enableLayoutReadyMixin) { this.hideLoader(); }
        }.bind(this)
      });
    },

    refreshData: function() {
      this.enableLayoutReadyMixin = false;
      this.loadData();
    },

    showLoader: function() {
      $(React.findDOMNode(this.refs.loader)).addClass('active').fadeIn();
    },

    hideLoader: function() {
      $(React.findDOMNode(this.refs.loader)).fadeOut();
    },

    componentDidMount: function() {
      // this.enableLayoutReadyMixin = false;
      this.loadData();
    },

    render: function() {

      var items = [];
      this.state.data.forEach(function(article){
        items.push(<ArticleItem article={article} key={article.id} thumb={this.props.thumb}/>);
      }.bind(this));
      return(
        <div className="container">
          <ul>{items}</ul>
          <div ref="loader" className="ui inverted dimmer">
              <div className="ui inverted indeterminate medium text loader">Loading</div>
          </div>
        </div>
      );
    }
  });
  return ArticleListBox;
});