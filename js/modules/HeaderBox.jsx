define([
  'jquery',
  'mod/login',
  'react',
  'mod/notification',
  'semantic',
  'facebook'
], function($, Login, React, Notification){

  /*
   * Sign In Button
   */
  var SigninButton = React.createClass({
    handleClick: function() {
      Notification.postNotification('kSigninDimmerRequestShowingNotification', null);
    },
    render: function() {
      return(<div className="ui signin button" onClick={this.handleClick}>SIGN IN / UP</div>);
    }
  })

  /*
   * Profile Box
   */
  var ProfileBox = React.createClass({
    handleClickSetting: function() {

    },

    handleClickSignout: function() {
      Login.logout();
      this.props.signoutCallback();
      Notification.postNotification('kUserLoggedOutNotification', null);
    },

    componentDidMount: function() {
      var $el = $(React.findDOMNode(this.refs.dropdown));
      $el.dropdown({
        action: 'hide',
        transition: 'scale'
      });
    },

    render: function() {

      var user = this.props.user;

      var imgUrl = '/images/no_profile@2x.png';
      if (user.hasOwnProperty('profileimage')){
        imgUrl = user.profileimage.url;
        if (user.profileimage.source == 'travelog'){
          imgUrl = imgUrl + '_small';
        }  
      }
      
      var name = user.name;
      if (!name) {
        name = user.lastname + ' ' + user.firstname;
      }

      return(
        <div className="menu">
          <div className="profile divider"></div>
          <img className="profile ui circular image" src={imgUrl} />
          <div className="profile name text">{name}</div>
          <div className="profile ui floating dropdown" ref="dropdown">
              <i className="dropdown icon"></i>
              <div className="menu">
                <div className="item" onClick={this.handleClickSetting}><i className="user icon"></i>Setting</div>
                <div className="item" onClick={this.handleClickSignout}><i className="sign out icon"></i>Logout</div>
              </div>
            </div>
        </div>
      );
    }
  });

  /*
   * Header Box
   */
  var HeaderBox = React.createClass({
    
    handleClickSignBtn: function() {
      Notification.postNotification('kSigninDimmerRequestShowingNotification');
    },

    getInitialState: function() {
      return { user: null };
    },

    handleSignin: function(user) {
      console.log(this.state);
      this.setState({ user: user});
    },

    handleSignout: function() {
      this.setState({ user: null});
    },

    componentDidMount: function() {
      Notification.registerNotification(this, 'kUserLoggedInNotification', this.handleSignin);
    },

    componentWillUnmount: function() {
      Notification.removeNotification(this, 'kUserLoggedInNotification');
    },

    render: function() {
      var loggedIn = this.state.user ? true : false;
      var profileBox = loggedIn ? <ProfileBox user={this.state.user} signoutCallback={this.handleSignout} /> : <SigninButton onClick={this.handleClickSignBtn} />;

      return(
        <div className="wrapper">
          <div className="logo-box">
              <a href="/">
                  <img src="/images/logo_transparent@2x.png" className="img" border="0" />
              </a>
              <img className="slogan" />
          </div>
          <div className="right-box ui large white circular labels">
              <a className="ui label" target="_blank" href="http://travelogme.wordpress.com/"><i>BLOG</i></a>
              <a className="ui label" target="_blank" href="https://facebook.com/travelogme"><i className="facebook icon"></i></a>
              <a className="ui label" target="_blank" href="http://instagram.com/travelog.me"><i className="instagram icon"></i></a>
              <a className="ui label" target="_blank" href="http://pinterest.com/travelogme"><i className="pinterest icon"></i></a>
              <a className="ui label" target="_blank" href="http://twitter.com/travelogme"><i className="twitter icon"></i></a>
              <a className="ui label" target="_blank" href="https://plus.google.com/+TravelogMe"><i className="google plus icon"></i></a>
              {profileBox}
          </div>
        </div>
      );
    }
  });

  return HeaderBox;
  
});