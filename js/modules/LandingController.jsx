define([
  'jquery',
  'react',
  'api',
  'jsx!mod/TopicBox',
  'jsx!mod/ArticleListBox',
  'jsx!mod/MainCitiesBox',
  'jsx!mod/MainVideoBox',
  'jsx!mod/NewsFeedBox',
  'mod/notification',
  'mod/LayoutReadyMixin',
  'css!css_/landing',
  'css!css_/newsfeed.section',
  'css!css_/promote.box',
  'css!css_/article.list.box',
  'css!root/js/lib/slick/slick',
  'css!root/js/lib/slick/slick-theme'
], function($, React, api, TopicBox, ArticleListBox, MainCitiesBox, MainVideoBox, NewsFeedBox, Notification, LayoutReadyMixin) {

  var LandingBox = React.createClass({
    mixins: [LayoutReadyMixin],

    handleClickRefreshPopularNow: function() {
      this.refs.popular_box.refreshData();
    },

    componentDidMount: function() {
      // for Layout Ready
      this.addSubModule(this.refs.topic_box);
      this.addSubModule(this.refs.main_city_box);
      this.addSubModule(this.refs.popular_box);
      this.addSubModule(this.refs.main_video_box);
      this.addSubModule(this.refs.news_feed_box);
    },

    render: function() {
      var popular_now_api_url = api.baseUrl + 'web/recent_articles';
      return (
        <div>
          <section>
              <div className="wrapper">
                  <div className="left-container">
                      <div className="recommended-article-box">
                        <TopicBox ref="topic_box" /> { /* Topic Box */ }
                      </div>
                      <div className="cities-box">
                        <MainCitiesBox ref="main_city_box" /> { /* Cities Box */ }
                      </div>
                  </div>
                  <div className="right-container">
                      <div className="article-list-box">
                          <h2>POPULAR NOW<a onClick={this.handleClickRefreshPopularNow}><i className="refresh icon"></i></a></h2>
                          <ArticleListBox ref="popular_box" url={popular_now_api_url} params={{limit:5}}/> { /* Popular Box */ }
                      </div>
                      <div className="download-app-box">
                          <span className="title">DOWNLOAD OUR APP</span>
                          <span className="description">HERE'S A SHORTCUT TO THE BEST PLACE TO STAY, EAT AND PLAY IN ASIA</span>
                          <div className="ui button">
                              <i className="apple icon"></i>GET THE APP
                          </div>
                      </div>
                      <div className="subscribe-promote-box">
                          <span className="description">SUBSCRIBE TO OUR NEWSLETTER & RECIEVE UPDATES RIGHT IN YOUR INBOX!</span>
                          <div className="ui mini left icon input">
                              <input type="text" placeholder="Email address" />
                              <i className="mail icon"></i>
                          </div>
                          <div className="ui travelog tiny subscribe button">SUBSCRIBE</div>
                      </div> 
                  </div>
              </div>
          </section>
          <section className="video">
            <MainVideoBox ref="main_video_box" /> { /* Main Video Box */ }
          </section>
          <section className="newsfeed">
            <NewsFeedBox ref="news_feed_box" /> { /* News Feed Box */ }
          </section>
        </div>
      );
    }
  });

  return LandingBox;
});