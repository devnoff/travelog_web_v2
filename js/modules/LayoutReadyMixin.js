define([
  'react',
  'mod/notification'
],
function(React, Notification){
  var LayoutReadyMixin = {
    addSubModule: function(subModule) {
      // Check the submodule has LayoutReadySubMixin through checking its property
      if (subModule.hasOwnProperty('isLayoutReadySubMixin')
        && subModule.isLayoutReadySubMixin){
        this.subModules.push(subModule);
        subModule.layoutSubmoduleReadyCallback = this.layoutReady;
      } else {
        console.error('subModule should be LayoutReadySubMixin');
      }
    },

    checkLayoutReady: function() {
      var o = this.subModules;
      for (var m in o){
        if (!o[m].isSubModuleLayoutReady()) {
          return false;
        }
      }
      return true;
    },

    layoutReady: function() {
      if (this.checkLayoutReady()){
        setTimeout(function(){
          Notification.postNotification('kMainPageReadyToShowNotification', this);  
        }.bind(this), 100);
        
      }
    },

    componentWillMount: function() {
      this.subModules = [];
    },

  };
  return LayoutReadyMixin;
});