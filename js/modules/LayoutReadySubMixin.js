define(['react']
, function(React) {
  var LayoutReadySubMixin = {
    
    subModuleLayoutReady: function(ready) {
      this.subModuleLayoutReady_ = ready;
    },

    isSubModuleLayoutReady: function() {
      return this.subModuleLayoutReady_;
    },

    componentDidUpdate: function() {
      if (this.subModuleLayoutReady 
        && this.enableLayoutReadyMixin
        && this.layoutSubmoduleReadyCallback) {
        this.layoutSubmoduleReadyCallback();
      }
    },

    componentWillMount: function() {
      this.isLayoutReadySubMixin = true,
      this.layoutSubmoduleReadyCallback = null,
      this.subModuleLayoutReady_ = false,
      this.enableLayoutReadyMixin = true;
    },

  };

  return LayoutReadySubMixin;
});