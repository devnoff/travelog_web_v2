define([
  'api', 
  'react',
  'mod/LayoutReadySubMixin'
  ], 
  function(api, React, LayoutReadySubMixin) {

  var CityItem = React.createClass({
    render: function() {
      var liStyle = {
        backgroundImage: 'url(' + (this.props.city.cover_image.url + '_large') + ')'
      };
      
      return(
        <li>
            <a href="#">
                <div className="bg" style={liStyle}></div>
                <span>{this.props.city.name.toUpperCase()}</span>
            </a>
        </li>
      );
    }
  });

  var MainCitiesBoxContainer = React.createClass({
    mixins: [LayoutReadySubMixin],

    getInitialState: function() {
      return {
        data: []
      };
    },

    finishLoading: function() {
      this.subModuleLayoutReady(true);

    },

    loadData: function() {
      var url = api.baseUrl + 'web/main_cities';
      $.ajax({
        url: url,
        dataType: 'json',
        success: function(data) {
          this.finishLoading();
          this.setState({
            data: data.data
          });
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(url, status, err.toString());
          this.finishLoading();
        }.bind(this)
      });
    },

    componentDidMount: function() {
      this.loadData();
    },

    render: function() {

      var items = [];
      this.state.data.forEach(function(city){
        items.push(<CityItem city={city} key={city.id} />);
      }.bind(this));
      return(
        <div className="container">
          <ul>{items}</ul>
        </div>
      );
    }
  });


  return MainCitiesBoxContainer;
});