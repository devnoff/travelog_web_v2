define([
  'api', 
  'react',
  'mod/LayoutReadySubMixin'
  ], 
  function(api, React, LayoutReadySubMixin) {


  var VideoItem = React.createClass({

    render: function() {
      var imgStyle = {
        backgroundImage: 'url(' + (this.props.video.content.thumbnail.url) + ')'
      };

      var liClass = this.props.selected ? 'selected' : '';
      
      return(
        <li onClick={this.props.onClick.bind(null, this)} className={liClass}>
            <div className="img" style={imgStyle}>
                <div className="play"><img src="/images/NewsFeed_Play_Small@2x.png" /></div>
            </div>
            <div className ="title">{this.props.video.content.title}</div>
        </li>
      );
    }
  });

  var MainVideoListContainer = React.createClass({

    getInitialState: function() {
      return { selectedIndex: 0 };
    },

    handleClick: function(video) {
      this.props.onSelectVideo(video);
      this.setState({selectedIndex: video.props.index});
    },

    render: function() {

      var items = [];
      this.props.videos.map(function(video, i){
        var selected = this.state.selectedIndex == i;
        items.push(<VideoItem video={video} key={i} onClick={this.handleClick} selected={selected} index={i}/>);
      }.bind(this));
      return(<ul>{items}</ul>);
    }
  });

  var MainVideoPlayContainer = React.createClass({

    handlePlay: function() {
      console.log('play btn tapped');
      var $embed = $('section.video .embed-box');
      $embed.video({
        id: this.props.video.content.identifier,
        source: 'youtube',
        autoplay: true
      }).show();

    },

    setEmbed: function() { 
      var $embed = $('section.video .embed-box');
      $embed.hide();

    },

    componentDidUpdate: function() {
      console.log('MainVideoPlayContainer componentDidUpdate');
      if (this.props.autoplay) {
        this.handlePlay();
      }
    },

    render: function() {
      console.log('play render');

      this.setEmbed();

      var bgStyle = {
        backgroundImage: 'url(' + this.props.video.content.thumbnail.url + ')'
      }
      return(
        <div className="video-play-box" style={bgStyle}>
            <div className="container">
                <div onClick={this.handlePlay}>
                    <div className="play-button">
                        <i className="video play icon"></i>
                    </div>
                    <div className="title">{this.props.video.content.title}</div>
                </div>
            </div>
            <div className="embed-box"></div>
        </div>
      );
    }
  });

  var MainVideoBoxContainer = React.createClass({
    mixins: [LayoutReadySubMixin],

    getInitialState: function() {
      return { selectedVideo : false, autoplay: false, data: [] };
    },

    handleSelectVideo: function(videoItem) {
      console.log('selected a video');

      this.setState({ selectedVideo : videoItem.props.video, autoplay: true });
    },


    loadData: function() {
      var url = api.baseUrl + 'web/recent_videos';
      $.ajax({
        url: url,
        data: {
          limit: 5
        },
        dataType: 'json',
        success: function(data) {
          this.subModuleLayoutReady(true);
          this.setState({
            data: data.data,
            selectedVideo: data.data[0]
          });
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(url, status, err.toString());
          this.subModuleLayoutReady(true);
        }.bind(this)
      });
    },

    componentDidMount: function() {
      this.loadData();
    },

    render: function() {
      console.log('video box render');

      var videoPlayBox = this.state.selectedVideo ? <MainVideoPlayContainer video={this.state.selectedVideo} autoplay={this.state.autoplay}/> : '';
      var videoListBox = this.state.data ? <MainVideoListContainer videos={this.state.data} onSelectVideo={this.handleSelectVideo} /> : '';
      return(
        <div className="wrapper">
            <h2>POPULAR VIDEOS</h2>
            <div className="left-container">
                { videoPlayBox }
            </div>
            <div className="right-container">
                <div className="video-list-box">
                    <div className="container">
                        { videoListBox }
                    </div>
                </div>
            </div>
            <div ref="loader" className="ui inverted dimmer"><div className="ui loader"></div></div>
        </div>
      );
    }
  });

  return MainVideoBoxContainer;
});