define([
  'jquery',
  'react',
  'semantic'
], function($, React){
    var NavBox = React.createClass({
      componentDidMount: function() {
        var $btn = $(React.findDOMNode(this.refs.cityBtn));
        var $menu = $(React.findDOMNode(this.refs.cityList));
        $btn
          .popup({
            popup : $menu,
            hoverable: true,
            inline: false,
            setFluidWidth: false,
            delay: {
              show: 300,
              hide: 800
            }
          });
      },

      render: function() {
        return (
          <div className="wrapper">
            <div className="container">
              <a ref="cityBtn" className="cities">CITIES<i className="caret down icon"></i></a>
              <a href="/bloggers" className="bloggers">BLOGGERS</a>
              <a className="empty"></a>
              <a href="/search" className="search"><i className="search icon"></i></a>
            </div>
              
            { /* Hidden City Menu Popup */ }
            <div ref="cityList" className="ui custom wide popup city-list">
                <h4>Popular Cities</h4>
                <ul>
                    <li><a href="/test"><span>Singapore</span><span>Singapore</span></a></li>
                    <li><a href="/a"><span>Bali</span><span>Indonesia</span></a></li>
                    <li><a href="/city/5835/kuala_lumpur-my"><span>Kuala Lumur</span><span>Malaysia</span></a></li>
                    <li><a href="/city/229/bangkok-th"><span>Bangkok</span><span>Thailand</span></a></li>
                    <li><a href="/city/312/phuket-th"><span>Phuket</span><span>Thailand</span></a></li>
                    <li><a href="/city/6037/chiang_mai-th"><span>Chiang Mai</span><span>Thailand</span></a></li>
                    <li><a href="/city/73/beijing-cn"><span>Beijing</span><span>China</span></a></li>
                    <li><a href="/city/253/shanghai-cn"><span>Shanghai</span><span>China</span></a></li>
                    <li><a href="/city/200/hong_kong-hk"><span>Hong Kong</span><span>Hong Kong</span></a></li>
                    <li><a href="/city/1/seoul-kr"><span>Seoul</span><span>South Korea</span></a></li>
                    <li><a href="/city/808/tokyo-jp"><span>Tokyo</span><span>Japan</span></a></li>
                </ul>
            </div>
          </div>
        );
      }
    });

  return NavBox;
});