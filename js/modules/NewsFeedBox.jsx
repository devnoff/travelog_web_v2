define([
  'api', 
  'react',
  'jquery',
  'masonry',
  'tweetlink',
  'mod/LayoutReadySubMixin',
  'dotdotdot',
  'semantic_comp/dropdown.min',
  'css!semantic_comp/dropdown.min',
  ], 
  function(api, React, $, Masonry, Tweetlink, LayoutReadySubMixin) {

  var FeedItem = React.createClass({

    handleVideoOpen: function() {
      var identifier = this.props.item.feed_content.identifier;
      var $modal = $('.ui.video.modal');
      var $embed = $('.ui.video.modal .video');
      $embed.video({
        id: identifier,
        source: 'youtube',
        autoplay: true
      });
      $modal.modal({
        onHidden: function() {
          $embed.video({});
        }
      }).modal('show');
    },

    render: function() {

      var item = this.props.item;
      var channel = this.props.item.channel;
      var account = this.props.item.account.name;
      var returnEl = '';
      var cityname = this.props.item.city.name;
      var coverUrl = '';

      if (channel === 'instagram') {
        coverUrl = this.props.item.feed_content.photo.hasOwnProperty('url') ? this.props.item.feed_content.photo.url : '';

        returnEl = (
          <li className="instagram sns item">
              <div className="city">
                  <span>{cityname.toUpperCase()}</span>
                  <i className="instagram icon"></i>   
              </div>
              <div className="img">
                  <img src="/images/empty_square.png"  style={{ backgroundImage: 'url(' + coverUrl + ')' }} />
              </div>
              <div className="title">
                  <div>
                      <div className="ui mini label">{account}</div>
                  </div>
              </div>
              <div className="message">
                  {this.props.item.feed_content.message}
              </div>
              <div className="tool">
                  <a><i className="bookmark icon"></i></a>
                  <a><i className="share icon"></i></a>
                  <a href={this.props.item.feed_content.url} target="_blank">View on Instagram</a>
              </div>    
          </li>
        );
      } else if (channel == 'twitter') {

        var linkified = Tweetlink.linkifyEntities(this.props.item.feed_content);
        var tweet = this.props.item.feed_content;
        var imgUrl = '';
        var imgStyle = { display: 'none' };
        if (tweet.entities.media && tweet.entities.media.length > 0) {
          imgUrl = tweet.entities.media[0].media_url + ':small';
          imgStyle = { backgroundImage : 'url(' + imgUrl + ')'};
        }

        returnEl = (
          <li className="twitter sns item">
              <div className="city">
                  <span>{cityname.toUpperCase()}</span>
                  <i className="twitter icon"></i>   
              </div>
              <div className="title">
                  <div>
                      <div className="ui mini label">{account}</div>
                      <div>
                        <span className="username">@{this.props.item.feed_content.author.username}</span>
                      </div>
                  </div>
              </div>
              <div className="description" dangerouslySetInnerHTML={{__html: linkified}}>
              </div>
              <div className="img" style={imgStyle}>
              </div>
              <div className="tool">
                  <a><i className="bookmark icon"></i></a>
                  <a><i className="share icon"></i></a>
                  <a href={this.props.item.feed_content.url} target="_blank">View on Twitter</a>
              </div>
          </li>
        );
      } else if (channel == 'facebook') {
        coverUrl = this.props.item.feed_content.photo.hasOwnProperty('url') ? this.props.item.feed_content.photo.url : '';

        returnEl = (
          <li className="facebook sns item">
              <div className="city">
                  <span>{cityname.toUpperCase()}</span>
                  <i className="facebook square icon"></i>   
              </div>
              <div className="img" style={{ backgroundImage: 'url(' + coverUrl + ')' }}>
              </div>
              <div className="title">
                  <div className="ui mini label">{account}</div>
              </div>
              <div className="message">
                  {this.props.item.feed_content.message}
              </div>
              <div className="tool">
                  <a><i className="bookmark icon"></i></a>
                  <a><i className="share icon"></i></a>
                  <a href={this.props.item.feed_content.url} target="_blank">View on Facebook</a>
              </div>
          </li>
        );
      } else if (channel == 'article') {
        coverUrl = this.props.item.article.cover_image.hasOwnProperty('url') ? this.props.item.article.cover_image.url : '';

        returnEl = (
          <li className="article item">
              <div className="city">
                  <span>{cityname.toUpperCase()}</span>
                  <i className="travelog icon"></i>   
              </div>
              <div className="img" style={{ backgroundImage: 'url(' + coverUrl + ')' }}>
              </div>
              <div className="title">
                  <div>
                      <div className="ui mini label">{account}</div>
                  </div>
                  <a href={'/article/' + this.props.item.article.id}>{this.props.item.article.title}</a>
              </div>
              <div className="description">
                  {this.props.item.article.description}
              </div>
              <div className="tool">
                  <a><i className="bookmark icon"></i></a>
                  <a><i className="share icon"></i></a>
                  <a href={'/article/' + this.props.item.article.id}>Read this Article</a>
              </div>
          </li>
        );
      } else if (channel == 'rss') {

        coverUrl = item.feed_content.photo && item.feed_content.photo.hasOwnProperty('url') 
                   ? this.props.item.feed_content.photo.url 
                   : '';
        returnEl = (
          <li className="rss item">
              <div className="city">
                  <span>{cityname.toUpperCase()}</span>
                  <i className="rss square icon"></i>   
              </div>
              <div className="img" style={{ backgroundImage: 'url(' + coverUrl + ')' }}>
                <i className="ellipsis horizontal icon"></i>
              </div>
              <div className="title">
                  <div>
                      <div className="ui mini label">{account}</div>
                  </div>
                  {this.props.item.feed_content.title}
              </div>
              <div className="description" dangerouslySetInnerHTML={{__html:this.props.item.feed_content.description}}>
              </div>
              <div className="tool">
                  <a><i className="bookmark icon"></i></a>
                  <a><i className="share icon"></i></a>
                  <a href={this.props.item.feed_content.url} target="_blank">Read this Article</a>
              </div>
          </li>
        );
      } else if (channel == 'youtube') {
        coverUrl = this.props.item.feed_content.thumbnail.hasOwnProperty('url') ? this.props.item.feed_content.thumbnail.url : '';

        returnEl = (
          <li className="youtube item">
              <div className="city">
                  <span>{cityname.toUpperCase()}</span>
                  <i className="youtube square icon"></i>   
              </div>
              <div className="img" style={{ backgroundImage: 'url(' + coverUrl + ')' }}>
                  <a onClick={this.handleVideoOpen}><img src="/images/NewsFeed_Play_Small@2x.png" /></a>
              </div>
              <div className="title">
                  <div>
                      <div className="ui mini label">{account}</div>
                  </div>
                  {this.props.item.feed_content.title}
              </div>
              <div className="tool">
                  <a><i className="bookmark icon"></i></a>
                  <a><i className="share icon"></i></a>
                  <a href={this.props.item.feed_content.url} target="_blank">Watch on YouTube</a>
              </div>
          </li>
        );
      }

      return returnEl;
    }
  });

  var NewsFeedBoxContainer = React.createClass({

    masonry : false,
    domChildren: [],
    reference: 'newsfeed_container',
    options: {
      "gutter": 10
    },
    offsets : false,
    
    mixins: [LayoutReadySubMixin],

    getInitialState: function() {
      return {data: []};
    },

    handleLoadMore: function() {
      this.enableLayoutReadyMixin = false;
      this.loadData();
    },

    updateStyle: function() {
      $('section.newsfeed .message').dotdotdot();
    },

    initializeMasonry: function(force) {
      if (!this.masonry || force) {
        this.masonry = new Masonry(this.refs[this.reference].getDOMNode(), this.options);
        this.domChildren = this.getNewDomChildren();
      }
    },

    getNewDomChildren: function () {
      var node = this.refs[this.reference].getDOMNode();
      var children = this.options.itemSelector ? node.querySelectorAll(this.options.itemSelector) : node.children;

      return Array.prototype.slice.call(children);
    },

    diffDomChildren: function() {
      var oldChildren = this.domChildren;
      var newChildren = this.getNewDomChildren();

      var removed = oldChildren.filter(function(oldChild) {
          return !~newChildren.indexOf(oldChild);
      });

      var added = newChildren.filter(function(newChild) {
          return !~oldChildren.indexOf(newChild);
      });

      var moved = [];

      if (removed.length === 0) {
          moved = oldChildren.filter(function(child, index) {
              return index !== newChildren.indexOf(child);
          });
      }

      this.domChildren = newChildren;

      return {
        old: oldChildren,
        'new': newChildren, // fix for ie8
        removed: removed,
        added: added,
        moved: moved
      };
    },

    performLayout: function() {
      var diff = this.diffDomChildren();

      if (diff.removed.length > 0) {
          this.masonry.remove(diff.removed);
          this.masonry.reloadItems();
      }

      if (diff.added.length > 0) {
          this.masonry.appended(diff.added);
      }

      if (diff.moved.length > 0) {
          this.masonry.reloadItems();
      }

      this.masonry.layout();
    },

    loadData: function() {

      var url = api.baseUrl + 'newsfeed';
      $.ajax({
        url: url,
        dataType: 'json',
        data: {
          limit: 15,
          travelzoo: false,
          offsets: this.offsets
        },
        success: function(data) {
          this.subModuleLayoutReady(true);

          this.offsets = data.metadata.offsets;
          
          var newData = this.state.data;
          newData = newData.concat(data.data);
          this.setState({data: newData});
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(url, status, err.toString());
          this.subModuleLayoutReady(true);
        }.bind(this)
      });
    },

    componentWillMount: function() {
      
    },

    componentDidMount: function() {
    
      this.initializeMasonry();
      this.performLayout();
      this.loadData();

      $('.ui.dropdown')
        .dropdown()
      ;
    },

    componentDidUpdate: function() {
      this.performLayout();
      this.updateStyle();
    },

    componentWillReceiveProps: function() {
      this._timer = setTimeout(function() {
        this.masonry.reloadItems();
        this.forceUpdate();
      }.bind(this), 0);
    },

    componentWillUnmount: function() {
      clearTimeout(this._timer);
    },

    render: function() {

      var items = [];
      this.state.data.map(function(item, i){
        items.push(<FeedItem item={item} key={i} />);
      }.bind(this));
      return(
        <div className="wrapper">
            <h2>
              NEWS FEED
            </h2>
            <div className="ui inline dropdown">
              <div className="text">World</div>
              <i className="dropdown icon"></i>
              <div className="menu">
                <div className="item" data-text="World">World</div>
                <div className="item" data-text="Singapore">Singapore</div>
                <div className="item" data-text="Bali">Bali</div>
                <div className="item" data-text="Kuala Lumur">Kuala Lumur</div>
                <div className="item" data-text="Bangkok">Bangkok</div>
                <div className="item" data-text="Phuket">Phuket</div>
                <div className="item" data-text="Chiang Mai">Chiang Mai</div>
                <div className="item" data-text="Beijing">Beijing</div>
                <div className="item" data-text="Shanghai">Shanghai</div>
                <div className="item" data-text="Hong Kong">Hong Kong</div>
                <div className="item" data-text="Seoul">Seoul</div>
                <div className="item" data-text="Tokyo">Tokyo</div>
              </div>
            </div>
            <ul id="newsfeed_container" ref={this.reference}>{items}</ul>
            <div className="fluid ui black button" onClick={this.handleLoadMore}>Load More</div>
            <div className="ui video modal">
              <i className="close icon"></i>
              <div className="video">
              </div>
            </div>
        </div>
      );
    }
  });

  return NewsFeedBoxContainer;
});