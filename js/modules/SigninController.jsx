define([
  'jquery',
  'react',
  'mod/login',
  'mod/notification'
], 
function($, React, Login, Notification) {
  var SigninController = React.createClass({

    getInitialState: function(){
      return {
        fb_loading: false,
        tw_loading: false,
        email_loading: false
      };
    },

    show: function() {
      var self = this;
      $('.ui.signin.dimmer')
        .dimmer('show')
        .dimmer({
          onHide:function(){
            self.resetEmailSignForm();      
          }
        });
    },

    hide: function() {
      $('.ui.signin.dimmer').dimmer('hide');
    },

    resetEmailSignForm: function() {
      React.findDOMNode(this.refs.email).value = '';
      React.findDOMNode(this.refs.password).value = '';
      var $el = $(React.findDOMNode(this.refs.emailSignBox));
      this.hideEmailSignForm($el);
    },

    handleLoggedIn: function(user) {
      Notification.postNotification('kUserLoggedInNotification', user);
      this.hide();
    },

    handleLoggedOut: function() {
      
    },

    resetState: function() {
      this.setState({ fb_loading: false, tw_loading: false, email_loading: false });
    },

    handleClickFacebookSign: function() {
      this.setState({ fb_loading: true });
      Login.withFacebook(function(error, user){
        if (user) {
          this.handleLoggedIn(user);  
        } else {
          alert(error.message);
        }
        this.resetState();
      }.bind(this));
    },

    handleClickTwitterSign: function() {
      this.setState({ tw_loading: true });
      Login.withTwitter(function(error, user){
        if (user) {
          this.handleLoggedIn(user);  
        } else {
          alert(error.message);
        }
        this.resetState();
      }.bind(this));
    },

    handleClickEmailSign: function() {
      var email = React.findDOMNode(this.refs.email).value;
      var password = React.findDOMNode(this.refs.password).value;

      // Validation Check
      if (!email) {
        alert('Please enter email');
        return;
      }

      if (!password) {
        alert('Please enter password');
        return;
      }

      this.setState({ email_loading: true });
      Login.withEmail(email, password, function(error, user){
        if (user) {
          this.handleLoggedIn(user);  
        } else {
          alert(error.message);
        }
        this.resetState();
      }.bind(this));
    },

    handleClickEmailFormIndicator: function() {
      // Email Form Visibility
      var $el = $(React.findDOMNode(this.refs.emailSignBox));
      var visible = $el.is(':visible');
        
      if (!visible) {
        this.showEmailSignForm($el);
      } else {
        this.hideEmailSignForm($el);
      }
    },

    showEmailSignForm: function($el) {
      var h = $el.height();

      $el.css({opacity:0});
      $el.height(0);
      $el.show();
      $el.animate({
        height: h
      }, 400, function() {
        $el.fadeTo('slow', 1);
      }); 
    },

    hideEmailSignForm: function($el, ignore_animation) {
      var h = $el.height();

      if (ignore_animation) {
        $el.css({
          opacity: 0,
        }).hide().height(h);
        return;
      }

      $el.css({opacity:1});
      $el.animate({
        height: 0,
        opacity:0
      }, 400, function() {
        $el.hide();
        $el.height(h);
      });
    },

    handleKeyPress: function(e) {
      if(e && (e.which == 13 || e.keyCode == 13))
      {
        this.handleClickEmailSign();
      }
    },

    componentDidMount: function() {
      Notification.registerNotification(this, 'kSigninDimmerRequestShowingNotification', this.show);
      Notification.registerNotification(this, 'kUserLoggedOutNotification', this.handleLoggedOut);
    },

    componentWillUnmount: function() {
      Notification.removeNotification(this, 'kSigninDimmerRequestShowingNotification');
      Notification.removeNotification(this, 'kUserLoggedOutNotification');
    },

    render: function(){
      var fb_loader = this.state.fb_loading;
      var tw_loader = this.state.tw_loading;
      var email_loader = this.state.email_loading;

      return(
        <div className="content">
            <div className="center">
                <img className="logo" src="/images/logo_inverted_symbol@2x.png" />
                <h2>Sign in to Travelog or create an account</h2>
                <p className="promote">Discover and share your travel experiences on the go. 
                Join a community of like-minded travellers, bloggers, local experts 
                and receive tailored recommendations on top things to do
                </p>
                <div>
                    <div>
                        <div ref="facebook_btn" className={"ui facebook button" + (fb_loader ? ' loading' : '')} onClick={this.handleClickFacebookSign}>
                            <i className="facebook icon"></i>
                            Sign in with Facebook
                        </div>
                    </div>
                    <div>
                        <div ref="twitter_btn" className={"ui twitter button" + (tw_loader ? ' loading' : '')} onClick={this.handleClickTwitterSign}>
                            <i className="twitter icon"></i>
                            Sign in with Twitter
                        </div>
                    </div>
                </div>
                <p className="extra">
                    We will never post to Facebook or Twitter without your permission. 
                    <br/>For old users <a className="email signin" onClick={this.handleClickEmailFormIndicator}>Sign in with Email</a>
                </p>
                <div className="email-sign-box" ref="emailSignBox" onKeyPress={this.handleKeyPress}>
                    <div>
                        <div className="ui mini input">
                            <input ref="email" name="email" type="email" placeholder="Email" />
                        </div>
                    </div>
                    <div>
                        <div className="ui mini input">
                            <input ref="password" name="password" type="password" placeholder="Password" />
                        </div>
                    </div>
                    <div>
                        <div className={"ui mini button" + (email_loader ? ' loading' : '')} onClick={this.handleClickEmailSign}>
                            Sign in with Email
                        </div>
                    </div>
                </div>
            </div>
        </div>
      );
    }
  });

  return SigninController;
});