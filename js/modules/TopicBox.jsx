define([
  'jquery',
  'api', 
  'react',
  'mod/LayoutReadySubMixin',
  'slick'
], 
  function($, api, React, LayoutReadySubMixin) {

  var ArticleItem = React.createClass({
    render: function() {
      var liStyle = {
        backgroundImage: 'url(' + this.props.article.cover_image.url + ')'
      };
      
      return(
        <div className="item" style={liStyle}>
            <div className="bg"></div>
            <a href={'/article/'+this.props.article.id}>
                <div className="ui mini label">{this.props.article.city.name.toUpperCase()}</div>
                <div>
                    <span>{this.props.article.title}</span>
                </div>
            </a>
        </div>
      );
    }
  });

  var TopicBoxContainer = React.createClass({

    mixins: [LayoutReadySubMixin],

    getInitialState: function() {
      return {
        data: []
      };
    },

    finishLoading: function() {
      this.applySlide();
    },

    loadData: function() {
      var url = api.baseUrl + 'web/topic';
      $.ajax({
        url: url,
        dataType: 'json',
        success: function(data) {
          this.subModuleLayoutReady(true);
          this.setState({
            data: data.data
          });
          this.finishLoading();
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(url, status, err.toString());
          this.subModuleLayoutReady(true);
          this.finishLoading();
        }.bind(this)
      });
    },

    applySlide: function() {
      var $el = $(React.findDOMNode(this.refs.container));

      try {
        $el.slick('unslick');
      } catch(e) {

      }

      $el.slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 3500,
        infinite: true,
        lazyLoad: 'progressive'
      });
    },

    componentDidMount: function() {
      this.loadData();
    },

    componentDidUpdate: function() {
      this.applySlide();
    },

    render: function() {

      var items = [];
      this.state.data.forEach(function(article){
        items.push(<ArticleItem article={article} key={article.id} />);
      }.bind(this));

      var loaderClass = "ui inverted dimmer" + (this.state.showLoader ? 'active' : '');
      return(
        <div ref="container" className="container">
          {items}
        </div>
      );
    }
  });

  return TopicBoxContainer;
});