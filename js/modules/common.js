require([
  'jquery',
  'semantic',
  'facebook'
], function($) {
  /*
   *
   * Setting Up Base User Interface
   *
   */


  // Facebook Sign in/up
  $('.signin.ui div.facebook.button').on('click', function(){
    console.log('facebook sign in');
    
    FB.getLoginStatus(function(response) {
      console.log(response);
      if (response.status === 'connected') {
        console.log('Logged in.');
      }
      else {
        FB.login(function(response){
          if (response.status === 'connected') {
            // Logged in
            console.log('Logged in.');
            console.log(response.authResponse);

          } else if (response.status === 'not_authorized') {
            // Need to show permission request
            console.log('not authorized');

          } else {
            // Nothing happen
            console.log('not logged in');
          }
        },  {scope: 'public_profile,email'});
      }
    });
  });
  
});