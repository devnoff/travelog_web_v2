define([
  'jquery',
  'api',
  'cookie',
  'facebook'
], function($, api, Cookie, FB){
  ___travelog_session_user = false;
  var instance = null;
  var cookie = new Cookie();
  var COOKIE_SESSION_KEY = '_travelog_session';

  var loadSession = function() {

  };

  var saveSession = function(token) {
    cookie.set(COOKIE_SESSION_KEY, token, {expires:1});
  };

  var clearSession = function() {
    cookie.del(COOKIE_SESSION_KEY);
  }

  function Login(){

    if(instance !== null){
      throw new Error("Cannot instantiate more than one MySingleton, use MySingleton.getInstance()");
    } 

    // Init Facebook
    FB.init({
      appId      : '420264061325667',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : false,  // parse social plugins on this page
      version    : 'v2.3' // use version 2.2
    });

  }

  Login.getInstance = function(){
      if(instance === null){
          instance = new Login();
      }
      return instance;
  };

  var setSessionUser = function(user) {
    ___travelog_session_user = user;
  };

  Login.prototype.getSessionUser = function() {
    return ___travelog_session_user;
  };

  /**
   * Check Valid Session
   *
   * @param {callback} callback - callback param : user 
   */

  Login.prototype.validSession = function(callback) {

    var session_token = cookie.get('_travelog_session');

    if (!session_token || session_token == undefined) {
      return callback(null, false);
    }

    var user = this.getSessionUser();
    if (user){
      return callback(null, user);
    }

    $.ajax({
      url: api.baseUrl + 'auth/login',
      dataType: 'json',
      method: 'post',
      data: {
        auth : 'token'
      },
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", "Bearer " + session_token);
      },
      success: function(data, status, xhr) {
        setSessionUser(data.user);
        saveSession(data.session.token);
        console.log(data);
        callback(null, data.user);
      },
      error: function(xhr, status, err) {
        console.error(status, err.toString());
        cookie.del('_travelog_session');
        callback(err, false);
      }
    });
  };



  Login.prototype.withEmail = function(email, password, callback) {
    var url = api.baseUrl + 'auth/login';
    var auth = {
      email: email,
      password: password
    };
    $.ajax({
      url: url,
      dataType: 'json',
      method: 'post',
      xhrFields: {
          withCredentials: true
      },
      data: {
        auth : 'email'
      },
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", "Basic " + btoa(auth.email + ":" + auth.password));
      },
      success: function(data, status, xhr) {
        setSessionUser(data.user);
        saveSession(data.session.token);
        console.log(data);
        callback(null, data.user);
      },
      error: function(xhr, status, err) {
        console.error(status, err.toString());
        callback(err, false);
      }
    });
  };

  Login.prototype.withFacebook = function(callback) {
    FB.getLoginStatus(function(response) {
      console.log(response);

      function apiAuth(userID, accessToken) {
        var url = api.baseUrl + 'auth/login';
        var auth = {
          auth: 'facebook',
          identifier: userID,
          token: accessToken
        };
        $.ajax({
          url: url,
          dataType: 'json',
          method: 'post',
          xhrFields: {
              withCredentials: true
          },
          data: auth,
          success: function(data, status, xhr) {
            setSessionUser(data.user);
            saveSession(data.session.token);
            console.log(data);
            callback(null, data.user);

            $('head').append('<meta property="fb:admins" content="'+response.authResponse.userID+'"/>');
          },
          error: function(xhr, status, err) {
            console.error(status, err.toString());
            callback(err, false);
            FB.logout(function(response) {
              console.log(response);
            });
          }
        });
      }

      if (response.status === 'connected') {
        console.log('Logged in.');
        apiAuth(response.authResponse.userID, response.authResponse.accessToken);
      }
      else {
        FB.login(function(response){
          if (response.status === 'connected') {
            // Logged in
            console.log('Logged in.');
            console.log(response.authResponse);
            apiAuth(response.authResponse.userID, response.authResponse.accessToken);

          } else if (response.status === 'not_authorized') {
            // Need to show permission request
            console.log('not authorized');
            callback({message:resonse.status}, false);

          } else {
            // Nothing happen
            console.log('not logged in');
            callback({message:'not_logged_in'}, false);
          }
        },  {scope: 'public_profile,email'});
      }
    }, true); // Force Check from Facebook
  };

  Login.prototype.withTwitter = function(callback) {
    function apiAuth(accessToken) {
        var url = api.baseUrl + 'auth/login';
        var token = {
          key: accessToken.oauth_token,
          secret: accessToken.oauth_token_secret
        };
        var auth = {
          auth: 'twitter',
          identifier: accessToken.user_id,
          token: JSON.stringify(token)
        };
        $.ajax({
          url: url,
          dataType: 'json',
          method: 'post',
          xhrFields: {
              withCredentials: true
          },
          data: auth,
          success: function(data, status, xhr) {
            setSessionUser(data.user);
            saveSession(data.session.token);
            console.log(data);
            callback(null, data.user);
          },
          error: function(xhr, status, err) {
            console.error(status, err.toString());
            callback(err, false);
          }
        });
      }

    window.__travelogTwitterAuthCallback = function(accessToken) {
      console.log(accessToken);
      apiAuth(accessToken);
    };
    console.log(window.__travelogTwitterAuthCallback);
    window.open('/twitter/auth.php', '__travelog_twitter_auth', "width=660,height=240");

  };

  Login.prototype.logout = function() {
    clearSession();
    setSessionUser(null);

    $('head meta[property="fb:admins"]').remove();
  };

  return Login.getInstance();
});