require([
  "react",
  "mod/session",
  "async",
  "backbone", 
  "mod/login",
  "jsx!mod/HeaderBox",
  "jsx!mod/NavBox",
  "jsx!mod/SigninController",
  "mod/notification",
  "util/StringHelper"
], function(React, Session, Async, Backbone, Login, HeaderBox, NavBox, SigninController, Notification) {

  // 새로고침 시 쿠키에 세션 존재 여부를 채크하여
  // 존재할 경우 사용자 정보를 불러오는 로직이 들어감
  // 사용자 정보를 불러오는 동안 백본 라우팅은 중지되며 로딩 인디케이터 출력
  // 사용자 정보 로드가 완료되면 정상적으로 라우팅 시작
  
  var router = null;

  /**
   * Show Page Loading Indicator
   * @return void
   */
  function showLoading() {
    
  }

  /**
   * Hide Page Loading Indicator
   * @return void
   */
  function hideLoading() {
    $('#pageLoading').fadeOut(); 
  }

  /**
   * Initialize Header View
   * @return void
   */
  function initializeHeader() {
    // Header Box
    HeaderBox = React.createFactory(HeaderBox);
    HeaderBox = React.render(HeaderBox({stx:this}), $('header')[0]);

    NavBox = React.createFactory(NavBox);
    NavBox = React.render(NavBox({stx:this}), $('nav')[0]);

    // Singin Controller
    var signinCallback = function(loggedIn){
      HeaderBox.handleSignin(loggedIn);
    }
    SigninController = React.createFactory(SigninController);
    SigninController = React.render(SigninController({ signinCallback: signinCallback }), $('.signin.ui.dimmer')[0]);
  }

  /**
   * Initialize Router Module
   * @return void
   */
  function initializeRouter() {

    require(['jsx!mod/router'], function(Router){
      router = Router;
      window.backbone_router = router;
    });
  }

  /**
   * Custom URL routing
   * @return void
   */
  function loadLinkHandlingBehavior() {
    $(document).on("click", "a", function(e) {
      var href = $(e.currentTarget).attr('href');

      if (href && !href.startsWith('http://') && !href.startsWith('https://')) {
        e.preventDefault(); 
        router.navigate(href, true); 
      }
    });
  }

  // Prepare to show
  showLoading();
  
  // Check Session Valid
  Login.validSession(function(error, user){

    console.log('main validSession');

    // Initialize Header View
    initializeHeader();

    // Initialize Router
    initializeRouter();

    // Settings
    loadLinkHandlingBehavior();

    // Loader
    hideLoading(); 
  });
});