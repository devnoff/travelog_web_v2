define([]
,function(){
  /*
   * notification structure
   * {
   *   'any key': [
   *     {
   *       observer: observer object,
   *       callback: observer callback
   *     }
   *   ]
   * } 
   */

  var notifications = {};
  function Notification(){

  }

  Notification.prototype.registerNotification = function(observer, key, callback) {

    var n = notifications[key];
    if (!n || !Array.isArray(n)) {
      n = new Array();
      notifications[key] = n;
    } 

    n.push({
      observer: observer,
      callback: callback
    });

    // console.log('registerNotification');
    // console.log(notifications);
  };

  Notification.prototype.removeAllNotification = function(key) {
    delete(notifications[key]);
  };

  Notification.prototype.removeNotification = function(observer, key) {
    var a = notifications[key];
    if (a && Array.isArray(a)) {
      var rmv_idx = -1;
      for (var i=0; i < a.length; i++){
        if (a[i].observer == observer){
          rmv_idx = i;
          break;
        }
      }
      a.splice(rmv_idx, 1);
    }
  };

  Notification.prototype.postNotification = function(key, object) {
    var n = notifications[key];
    if (!n || !Array.isArray(n)) {
      console.log('undefined notification key:'+key);
      return;
    }

    for (var i in n) {
      n[i].callback(object);
    }

    console.log('Post:' + key);
    
  };

  if (!window.travelog_notification || window.travelog_notification == undefined) {
    window.travelog_notification = new Notification();
  }

  return window.travelog_notification;
})