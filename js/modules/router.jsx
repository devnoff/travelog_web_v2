define([
  "jquery",
  "react",
  "backbone",
  "mod/notification"
], function($, React, Backbone, Notification) {
  var router = null;

  var $wrapperDOM = $('.body_wrapper');
  var $contentDOM = $('main');
  var cache = {};
  var $loader = $('#bodyLoading');
  var currentDelegate = null;
  var onPopState = false;

  function PageLoader() {

    this.registerPageReadyObserver();
  }

  PageLoader.prototype.showLoader = function() {
    $loader.addClass('active').show();
    $('body').css({overflow: 'hidden'});
  };

  PageLoader.prototype.displayPage = function() {
    $loader.fadeOut();
    $('body').css({overflow: 'scroll'});
  };


  PageLoader.prototype.loadPage = function(factory, options, key) {

    

    // Prevent crazy click
    $wrapperDOM.clearQueue();

    // React.unmountComponentAtNode($contentDOM[0]);
    
    if (currentDelegate) {
      var pos = $('body').scrollTop();
      currentDelegate.scrollPosition = pos;
    }

    $contentDOM.empty();

    if (cache[key]) {
      // Cache data load
      
      this.showLoader();
      
      $contentDOM.append($(cache[key]['dom']));
      currentDelegate = cache[key]['delegate'];
      currentDelegate.forceUpdate();

      if (onPopState) {
        onPopState = false;
        if (currentDelegate.hasOwnProperty('scrollPosition')) {
          $('html,body').scrollTop(currentDelegate.scrollPosition);  
        }  
      } else {
        $('html,body').scrollTop(0);
      }
      
      this.displayPage();

    } else {
      // Load new page
      
      this.showLoader();

      currentDelegate = React.render(factory(options), $contentDOM[0]);  
      // Caching
      var dom = $contentDOM.find('div:first-child')[0];
      cache[key] = {};
      cache[key]['dom'] = dom;
      cache[key]['delegate'] = currentDelegate;

      $('html,body').scrollTop(0);
    }

  }

  PageLoader.prototype.registerPageReadyObserver = function() {
    Notification.registerNotification(this, 'kMainPageReadyToShowNotification', function(object){
      if (currentDelegate == object) {
        this.displayPage();
      }
    }.bind(this));
  };

  var pageLoader = new PageLoader();
  

  var Router = Backbone.Router.extend({
    // Routing Rules
    routes : {
      ""          : "index",
      "test"      : "test",
      "a"         : "a",
      "article/:id"   : "article",
      "article/:id/"   : "article",
      "article/:id/:title"   : "article",
      "*notFound" : "notFound"
    },
    index : function() {
      require(['jsx!mod/LandingController'], function(Landing){
        var LandingController = React.createFactory(Landing);
        pageLoader.loadPage(LandingController, {}, 'index');
      });
    },
    test : function() {
      require(['jsx!mod/test'], function(Test){
        pageLoader.loadPage(React.createFactory(Test), {valStr:'Test'},Backbone.history.getFragment());
      });
    },
    a : function() {
      require(['jsx!mod/test'], function(Test){
        pageLoader.loadPage(React.createFactory(Test), {valStr:'a'}, Backbone.history.getFragment());
      });
    },
    article: function(id, title) {
      require(['jsx!mod/ArticleController'], function(Article){
        pageLoader.loadPage(React.createFactory(Article), {article_id:id}, 'article/'+id);
        console.log('route: '+ Backbone.history.getFragment());
      });
    },
    notFound: function() {
      require(['jsx!mod/test'], function(Test){
        pageLoader.loadPage(React.createFactory(Test), {valStr:'Page not found'}, Backbone.history.getFragment());
      });
    }
  });

  

  router = new Router();
  router.on("route", function(route, params) {
    console.log("Different Page: " + route);
  });

  Backbone.history.start({
    pushState: true
  });


  window.onpopstate = function(){
    console.log('back');
    onPopState = true;
  };


  return router;
});