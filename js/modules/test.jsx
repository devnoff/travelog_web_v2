define([
  'react',
  'mod/notification'
], 
function(React, Notification){
	var Test = React.createClass({
		componentDidMount: function() {

      setTimeout(function() {
        Notification.postNotification('kMainPageReadyToShowNotification', this);
      }.bind(this), 400);
		},

		render: function() {
			return(
        <div>Hello world! {this.props.valStr}</div>
      );
		}
	});
  return Test;
});