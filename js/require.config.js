var require = {
        baseUrl: '/js',
        paths: {
            api: 'modules/travelog',
            jquery: 'lib/jquery-2.1.3.min',
            dotdotdot: 'lib/jquery.dotdotdot',
            semantic: 'lib/semantic-ui/semantic',
            slick: '../../node_modules/slick-carousel/slick/slick',
            react: 'lib/react-with-addons',
            jsx: 'lib/jsx',
            JSXTransformer: 'lib/JSXTransformer',
            showdown: 'lib/showdown.min',
            text: 'lib/text',
            Utils: 'lib/Utils',
            masonry: 'lib/masonry.pkgd',
            tweetlink: 'lib/twitter-entities',
            facebook: '//connect.facebook.net/en_US/sdk',
            backbone: 'lib/backbone-min',
            underscore: 'lib/underscore-min',
            async: '../../node_modules/async/lib/async',
            cookie: '../../node_modules/browser-cookie/lib/cookie',
            // Helper Paths
            mod: 'modules',
            tmpl: '../template',
            css_: '../css',
            root: '../',
            semantic_comp: 'lib/semantic-ui/components'
        },
        shim: {
            semantic: ['jquery'],
            semantic_comp: ['semantic'],
            JSXTransformer: {
                exports: "JSXTransformer"
            },
            slick: ['jquery'],
            dotdotdot: ['jquery'],
            masonry: ['jquery'],
            tweetlink: ['jquery'],
            facebook : {
              exports: 'FB'
            },
            backbone : {
              deps : [
                "jquery",
                "underscore"
              ],
              exports : "Backbone"
            },
            underscore : {
              "exports" : "_"
            },
            cookie: {
                "exports" : "Cookie"
            }
        },
        jsx: {
            fileExtension: '.jsx'
        },
        map: {
          '*': {
            'css': '../../node_modules/require-css/css' 
          }
        }
    };