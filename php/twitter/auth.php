<?php 

define('__ROOT__', dirname(dirname(__FILE__))); 
require_once(__ROOT__.'/lib/twitter/Twitteroauth.php');
require_once(__ROOT__.'/lib/twitter/twitter.config.php');

$twitteroauth = new TwitterOAuth();

$consumer_token = $config['twitter_consumer_token'];
$consumer_secret = $config['twitter_consumer_secret'];

$oauth_token = $_GET["oauth_token"];

if ($oauth_token) { 
    // Load from callback
    
    $request_token = $_COOKIE['__travelog_twitter_session_token'];
    $request_token_secret = $_COOKIE['__travelog_twitter_session_token_secret'];

    // Delete Cookie
    setcookie("__travelog_twitter_session_token", "", time() - 3600);
    setcookie("__travelog_twitter_session_token_secret", "", time() - 3600);

    echo 'Consumer Token & Secret : '.$consumer_token.' '.$consumer_secret.'<br/>';
    echo 'Cookies : '.$request_token.' '.$request_token_secret.'<br/>';

    $connection = $twitteroauth->create($consumer_token, $consumer_secret, $request_token, $request_token_secret);
    $access_token = $connection->getAccessToken($_GET['oauth_verifier']);
    $access_token_str = json_encode($access_token);

    echo "<script>window.opener.__travelogTwitterAuthCallback($access_token_str); window.close();</script>";

} else {
    // 처음 로그인 시    
    $connection = $twitteroauth->create($consumer_token, $consumer_secret);

    $callback_url = "http://$_SERVER[HTTP_HOST]/twitter/auth.php";
    $request_token = $connection->getRequestToken($callback_url);

    $twitter_session = array(
        'request_token' => $request_token['oauth_token'],
        'request_token_secret' => $request_token['oauth_token_secret']
    );

    setcookie('__travelog_twitter_session_token', $twitter_session['request_token'], time() + (60*5));
    setcookie('__travelog_twitter_session_token_secret', $twitter_session['request_token_secret'], time() + (60*5));

    // echo 'Consumer Token & Secret : '.$consumer_token.' '.$consumer_secret.'<br/>';
    // var_dump($twitter_session);
     
    if($connection->http_code == 200) {
        $url = $connection->getAuthorizeURL($request_token);
        // echo $url;
        header("Location: ".$url);
        die();
    } 
}

return;


?>