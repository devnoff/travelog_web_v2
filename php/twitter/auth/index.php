<?php 

define('__ROOT__', dirname(dirname(__FILE__))); 
require_once(__ROOT__.'/lib/twitter/Twitteroauth.php');
require_once(__ROOT__.'/lib/twitter/twitter.config.php');

$twitteroauth = new TwitterOAuth();

$consumer_token = $config['twitter_consumer_token'];
$consumer_secret = $config['twitter_consumer_secret'];

$oauth_token = $_GET["oauth_token"];

if ($oauth_token) { 
    // Load from callback
    
    $request_token = $_COOKIE['__travelog_twitter_session_token'];
    $request_token_secret = $_COOKIE['__travelog_twitter_session_token_secret'];

    // Delete Cookie
    setcookie("__travelog_twitter_session_token", "", time() - 3600);
    setcookie("__travelog_twitter_session_token_secret", "", time() - 3600);

    echo 'Consumer Token & Secret : '.$consumer_token.' '.$consumer_secret.'<br/>';
    echo 'Cookies : '.$request_token.' '.$request_token_secret.'<br/>';

    $connection = $twitteroauth->create($consumer_token, $consumer_secret, $request_token, $request_token_secret);

    $access_token = $connection->getAccessToken($_GET['oauth_verifier']);

    // var_dump($access_token);
    echo json_decode($access_token);
} else {
    // 처음 로그인 시    
    $connection = $twitteroauth->create($consumer_token, $consumer_secret);

    $callback_url = "http://$_SERVER[HTTP_HOST]/twitter/auth.php";
    $request_token = $connection->getRequestToken($callback_url);

    $twitter_session = array(
        'request_token' => $request_token['oauth_token'],
        'request_token_secret' => $request_token['oauth_token_secret']
    );

    setcookie('__travelog_twitter_session_token', $twitter_session['request_token'], time() + (60*5));
    setcookie('__travelog_twitter_session_token_secret', $twitter_session['request_token_secret'], time() + (60*5));

    // echo 'Consumer Token & Secret : '.$consumer_token.' '.$consumer_secret.'<br/>';
    // var_dump($twitter_session);
     
    if($connection->http_code == 200) {
        $url = $connection->getAuthorizeURL($request_token);
        // echo $url;
        header("Location: ".$url);
        die();
    } 
}

return;




if ($oauth_token && $oauth_token === $request_token) {
    // 세션에 트위터 엑세스 토큰 정보가 있을 경우

    $connection = $this->twitteroauth->create($consumer_token, $consumer_secret, $request_token, $request_token_secret);

    $access_token = $connection->getAccessToken($this->input->get('oauth_verifier'));
    $user_id = $access_token['user_id'];
    if ($user_id) {
        $user = $this->twitteroauth->get('users/show', array('user_id' => $user_id));
    }
} else {
    
}

?>